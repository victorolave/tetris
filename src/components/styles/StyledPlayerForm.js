import styled from 'styled-components';

export const StyledPlayerForm = styled.div`
  
`

export const StyledNameInput = styled.input`
    padding: 10px;
    border-radius: 10px;
    margin: 0 0 10px 0;
`;

export const StyledTitle = styled.h2`
  color: white;
  font-family: Pixel, Arial, Helvetica, sans-serif;
`;
