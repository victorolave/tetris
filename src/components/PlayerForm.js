import React from "react";
import { StyledPlayerForm, StyledNameInput } from './styles/StyledPlayerForm';
import { StyledSaveButton } from './styles/StyledSaveButton';

const PlayerForm = ({ callback, handlePlayerName }) => (
    <StyledPlayerForm>
        <StyledNameInput onChange={handlePlayerName} type="text" placeholder="Nombre"/>
        <StyledSaveButton onClick={callback}>Guardar Puntaje</StyledSaveButton>
    </StyledPlayerForm>
);

export default PlayerForm;