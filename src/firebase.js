import firebase from 'firebase/app'
import 'firebase/firestore';

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyBoU4OfEnyIwGc2rhfkDyGH61AbMfJiFJk",
    authDomain: "react-tetris-c2b94.firebaseapp.com",
    databaseURL: "https://react-tetris-c2b94.firebaseio.com",
    projectId: "react-tetris-c2b94",
    storageBucket: "react-tetris-c2b94.appspot.com",
    messagingSenderId: "83305110990",
    appId: "1:83305110990:web:44e06458418abe9b24df9f"
};
// Initialize Firebase
const fb = firebase.initializeApp(firebaseConfig);

export const db = fb.firestore();